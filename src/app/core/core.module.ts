import { NgModule } from '@angular/core';

import { TransactionStore } from 'src/app/core/store/transaction/transaction.store';

@NgModule({
  providers: [
    TransactionStore
  ]
})
export class CoreModule {}
