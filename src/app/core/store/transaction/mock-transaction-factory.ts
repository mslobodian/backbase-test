import { TransferRequest, Transaction } from 'src/app/core/store/transaction/entity';

import * as transactions from '../../../../../mock/transactions.json';

const getLogo = (merchant: string): string => {
  const { merchantLogo } = transactions.data.find((transaction: Transaction) => transaction.merchant === merchant) || {
    merchantLogo: 'https://picsum.photos/100'
  };

  return merchantLogo;
};

const getCategoryCode = ((color: string[]) =>
  () => color[Math.floor(Math.random() * color.length)]
)(transactions.data.map(({ categoryCode }) => categoryCode));

export const createTransaction = ({ amount, beneficiary, transactionType }: TransferRequest): Transaction =>
  new Transaction(
    amount,
    beneficiary,
    getLogo(beneficiary),
    transactionType,
    Date.now(),
    getCategoryCode()
  );
