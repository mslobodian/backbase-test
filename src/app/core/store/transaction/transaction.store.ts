import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { TransferRequest, Transaction, TransactionSearch } from './entity';
import { createTransaction } from './mock-transaction-factory';
import { searchTransactions } from './transaction.search';

import * as mockTransactions from '../../../../../mock/transactions.json';

class TransactionState {
  balance = 5824.54;
  transactions: Transaction[] = mockTransactions.data as any;
  search: TransactionSearch = new TransactionSearch();
}

export class TransactionStore {
  private broadcast = new BehaviorSubject(new TransactionState());

  get$(): Observable<TransactionState> {
    return this.broadcast.asObservable();
  }

  update(data: Partial<TransactionState>): void {
    this.broadcast.next({ ...this.get(), ...data });
  }

  getTransactions$(): Observable<Transaction[]> {
    return this.get$().pipe(
      map(({ transactions, search }: TransactionState) => searchTransactions(transactions, search))
    );
  }

  getSearch$(): Observable<TransactionSearch> {
    return this.get$().pipe(
      map(({ search }: TransactionState) => search)
    );
  }

  getBalance(): number {
    return this.get().balance;
  }

  addTransaction(newTransaction: TransferRequest): Observable<Transaction> {
    return of(newTransaction).pipe(
      map(createTransaction),
      tap(
        (transaction: Transaction) => this.update({
          balance: this.get().balance - parseFloat(transaction.amount),
          transactions: [...this.get().transactions, transaction]
        })
      )
    );
  }

  addSearch(search: TransactionSearch): void {
    this.update({ search });
  }

  private get(): TransactionState {
    return this.broadcast.getValue();
  }
}
