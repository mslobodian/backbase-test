export enum SearchOrderType {
  None = '',
  ASC = 'asc',
  DESC = 'desc',
}

export enum SearchOrderBy {
  Date = 'date',
  Amount = 'amount',
  Beneficiary = 'beneficiary',
}

export class TransactionSearchOrder {
  readonly [SearchOrderBy.Date]: SearchOrderType = SearchOrderType.DESC;
  readonly [SearchOrderBy.Amount]: SearchOrderType = SearchOrderType.DESC;
  readonly [SearchOrderBy.Beneficiary]: SearchOrderType = SearchOrderType.DESC;
}

export class TransactionSearch {
  constructor(
    public readonly query = '',
    public readonly order: TransactionSearchOrder = new TransactionSearchOrder()
  ) {}
}
