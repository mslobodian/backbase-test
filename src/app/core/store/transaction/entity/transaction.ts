import { TransactionType } from './transaction-type';

export class TransferRequest {
  constructor(
    public readonly amount: string,
    public readonly benefactor: string, // from account
    public readonly beneficiary: string, // to account
    public readonly transactionType: TransactionType = TransactionType.OnlineTransfer,
  ) {}
}

export class Transaction {
  constructor(
    public readonly amount: string,
    public readonly merchant: string,
    public readonly merchantLogo: string,
    public readonly transactionType: TransactionType,
    public readonly transactionDate: number,
    public readonly categoryCode: string
  ) {}
}
