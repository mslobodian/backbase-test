import flow from 'lodash/fp/flow';
import filter from 'lodash/fp/filter';
import orderBy from 'lodash/fp/orderBy';

import startOfDay from 'date-fns/startOfDay';

import { Transaction, TransactionSearch, SearchOrderBy } from './entity';

const orderFieldMapping = (order: SearchOrderBy) => (transaction: Transaction) => ({
  [SearchOrderBy.Date]: startOfDay(transaction.transactionDate),
  [SearchOrderBy.Amount]: parseInt(transaction.amount, 10),
  [SearchOrderBy.Beneficiary]: transaction.merchant,
}[order]);

export const searchTransactions = (transactions: Transaction[], search: TransactionSearch): Transaction[] => flow(
  filter((transaction: Transaction) => [
    transaction.amount,
    transaction.merchant,
    transaction.transactionType,
  ].some((field: string) => field.toLowerCase().includes(search.query.toLowerCase()))),
  orderBy(
    [
      orderFieldMapping(SearchOrderBy.Date),
      orderFieldMapping(SearchOrderBy.Amount),
      orderFieldMapping(SearchOrderBy.Beneficiary)
    ],
    [
      search.order[SearchOrderBy.Date] as 'asc' | 'desc',
      search.order[SearchOrderBy.Amount] as 'asc' | 'desc',
      search.order[SearchOrderBy.Beneficiary] as 'asc' | 'desc'
    ]
  )
)(transactions) as Transaction[];
