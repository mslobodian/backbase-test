import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { CoreModule } from 'src/app/core/core.module';
import { AppRouting } from 'src/app/app.routing';
import { modules } from 'src/app/modules';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRouting,
    CoreModule,
    ...modules
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
