import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';

import { HomeRoutingModule } from 'src/app/modules/home/home.routing';

import { HomeMainComponent } from 'src/app/modules/home/main/home-main.component';
import { TransferFormComponent } from 'src/app/modules/home/aside/transfer-form/transfer-form.component';
import { TransactionListComponent } from 'src/app/modules/home/main/transaction-list/transaction-list.component';
import { TransactionListEmptyComponent } from 'src/app/modules/home/main/transaction-list-empty/transaction-list-empty.component';
import { TransactionItemComponent } from 'src/app/modules/home/main/transaction-item/transaction-item.component';
import { TransactionsFilterComponent } from './main/transactions-filter/transactions-filter.component';
import { TransferConfirmComponent } from './aside/transfer-confirm/transfer-confirm.component';


@NgModule({
  declarations: [
    HomeMainComponent,
    TransferFormComponent,
    TransactionListComponent,
    TransactionListEmptyComponent,
    TransactionItemComponent,
    TransactionsFilterComponent,
    TransferConfirmComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    HomeRoutingModule
  ]
})
export class HomeModule {}
