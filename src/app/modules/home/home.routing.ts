import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent, LayoutOutlet } from 'src/app/shared/layout/layout.component';

import { HomeMainComponent } from 'src/app/modules/home/main/home-main.component';
import { TransferFormComponent } from 'src/app/modules/home/aside/transfer-form/transfer-form.component';
import { TransferConfirmComponent } from 'src/app/modules/home/aside/transfer-confirm/transfer-confirm.component';
import { TransferConfirmGuard } from 'src/app/modules/home/aside/transfer-confirm/transfer-confirm.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        outlet: LayoutOutlet.Main,
        component: HomeMainComponent
      },
      {
        path: '',
        children: [
          {
            path: '',
            component: TransferFormComponent
          },
          {
            path: 'confirm',
            component: TransferConfirmComponent,
            canActivate: [TransferConfirmGuard],
          },
        ]
      },
    ]
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [
    TransferConfirmGuard
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
