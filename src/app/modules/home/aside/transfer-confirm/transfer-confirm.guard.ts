import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class TransferConfirmGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(): boolean {
    const hasTransfer = !!this.router.getCurrentNavigation().extras.state?.transfer;

    if (!hasTransfer) {
      this.router.navigate(['../']);
    }

    return hasTransfer;
  }
}
