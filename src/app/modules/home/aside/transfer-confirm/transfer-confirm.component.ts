import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { TransferRequest } from 'src/app/core/store/transaction/entity';
import { TransactionStore } from 'src/app/core/store/transaction/transaction.store';

@Component({
  selector: 'app-transfer-confirm',
  templateUrl: './transfer-confirm.component.html',
  styleUrls: ['./transfer-confirm.component.scss']
})
export class TransferConfirmComponent {

  readonly title = 'Confirm transfer';
  readonly transfer: TransferRequest = window.history.state.transfer;

  constructor(
    private router: Router,
    private location: Location,
    private transactionStore: TransactionStore
  ) {}

  handleBackNavigation(): void {
    this.router.navigate(['../'], {
      state: { transfer: this.transfer }
    });
  }

  handleTransferConfirm(): void {
    this.location.back();

    this.transactionStore.addTransaction(this.transfer)
      .subscribe();
  }
}
