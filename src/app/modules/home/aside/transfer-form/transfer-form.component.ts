import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TransferRequest } from 'src/app/core/store/transaction/entity';
import { TransactionStore } from 'src/app/core/store/transaction/transaction.store';

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransferFormComponent implements OnInit {

  form: FormGroup;
  readonly title = 'Make a transfer';
  readonly balance = this.transactionStore.getBalance();
  readonly creditLimit = -500;

  private readonly defaultFormValues: TransferRequest =
    window.history.state.transfer || new TransferRequest('', 'Free Checking (4692)', '');

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private transactionStore: TransactionStore,
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  handleSubmit(): void {
    const { amount, toAccount, fromAccount } = this.form.value;
    const transfer = new TransferRequest(
      amount,
      fromAccount,
      toAccount
    );

    this.router.navigate(['./confirm'], {
      relativeTo: this.route,
      state: { transfer }
    });
  }

  private buildForm(): void {
    const { amount, benefactor, beneficiary } = this.defaultFormValues;

    this.form = this.formBuilder.group({
      amount: [amount, [Validators.required, transferAmountCreditLimit(this.balance, this.creditLimit)]],
      toAccount: [beneficiary, [Validators.required]],
      fromAccount: [benefactor, [Validators.required]]
    });
  }
}

export function transferAmountCreditLimit(balance: number, credit: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    const inputAmount = parseFloat(control.value);

    if (!inputAmount) {
      return null;
    }

    const remainingAmount = balance - inputAmount;
    return remainingAmount > credit ? null : { transferAmountCreditLimitExceed: true };
  };
}
