import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-transaction-list-empty',
  template: '<p class="transaction-list-empty">No transactions found</p>',
  styleUrls: ['./transaction-list-empty.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionListEmptyComponent {}
