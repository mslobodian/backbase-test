import { ChangeDetectionStrategy, Component } from '@angular/core';

import { TransactionStore } from 'src/app/core/store/transaction/transaction.store';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeMainComponent {

  readonly title = 'Recent transactions';
  readonly search$ = this.transactionStore.getSearch$();
  readonly transactions$ = this.transactionStore.getTransactions$();

  constructor(
    private transactionStore: TransactionStore
  ) {}
}
