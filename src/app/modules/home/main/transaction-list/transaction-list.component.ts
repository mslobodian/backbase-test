import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Transaction } from 'src/app/core/store/transaction/entity';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionListComponent {
  @Input() transactions: Transaction[];
}
