import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import {
  SearchOrderBy,
  SearchOrderType,
  TransactionSearch
} from 'src/app/core/store/transaction/entity';
import { TransactionStore } from 'src/app/core/store/transaction/transaction.store';
import { DefaultToggleOption } from 'src/app/shared/form/button-toggle/button-toggle.component';

@Component({
  selector: 'app-transactions-filter',
  templateUrl: './transactions-filter.component.html',
  styleUrls: ['./transactions-filter.component.scss'],
  viewProviders: [
    {
      provide: DefaultToggleOption,
      useValue: {
        None: SearchOrderType.None,
        On: SearchOrderType.DESC,
        Off: SearchOrderType.ASC
      }
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionsFilterComponent implements OnInit, OnDestroy {
  form: FormGroup;
  formSubscription = new Subscription();

  @Input() search: TransactionSearch;

  constructor(
    private formBuilder: FormBuilder,
    private transactionStore: TransactionStore
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  ngOnDestroy(): void {
    this.formSubscription.unsubscribe();
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      query: this.search.query,
      [SearchOrderBy.Date]: this.search.order[SearchOrderBy.Date],
      [SearchOrderBy.Amount]: this.search.order[SearchOrderBy.Amount],
      [SearchOrderBy.Beneficiary]: this.search.order[SearchOrderBy.Beneficiary],
    });

    this.formSubscription = this.form.valueChanges.subscribe((formValues) => {
      const { query, ...order } = formValues;

      this.transactionStore.addSearch(new TransactionSearch(
        formValues.query,
        order
      ));
    });
  }
}
