import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Transaction } from 'src/app/core/store/transaction/entity';

@Component({
  selector: 'app-transaction-item',
  templateUrl: './transaction-item.component.html',
  styleUrls: ['./transaction-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionItemComponent {

  @Input() transaction: Transaction;
}
