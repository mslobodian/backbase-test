import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tile-box',
  templateUrl: './tile-box.component.html',
  styleUrls: ['./tile-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TileBoxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
