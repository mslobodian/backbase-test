import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-tile-heading',
  templateUrl: './tile-heading.component.html',
  styleUrls: ['./tile-heading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TileHeadingComponent {
  @Input()
  title: string;

  @Input()
  icon: string;
}
