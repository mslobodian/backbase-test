import { Component, Input } from '@angular/core';
import { ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-input-error',
  template: '<p *ngIf="isVisible"><ng-content></ng-content></p>'
})
export class InputErrorComponent {
  @Input()
  controlName: string;
  @Input()
  errorName: string;

  constructor(
    private controlContainer: ControlContainer
  ) {}

  get isVisible(): boolean {
    const control = this.controlContainer.control.get(this.controlName);
    return control.dirty && control.errors?.[this.errorName];
  }
}
