import {
  Component,
  Inject,
  AfterContentInit,
  Renderer2,
  DEFAULT_CURRENCY_CODE,
  ChangeDetectionStrategy,
  ContentChild,
  HostBinding
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { getCurrencySymbol } from '@angular/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { InputRefDirective } from '../input-ref/input-ref.directive';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputComponent,
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements AfterContentInit {
  @ContentChild(InputRefDirective) private control: InputRefDirective;

  isClearable$: Observable<boolean>;

  constructor(
    private renderer: Renderer2,
    @Inject(DEFAULT_CURRENCY_CODE) private currencyCode
  ) {}

  ngAfterContentInit(): void {
    this.isClearable$ = this.control.value$.pipe(map(Boolean));
  }

  get isCurrencyType(): boolean {
    return this.control.isCurrencyType;
  }

  get currencySymbol(): string {
    return getCurrencySymbol(this.currencyCode, 'narrow');
  }

  handleClear(): void {
    this.control.clear();
  }

  get focus(): boolean {
    return this.control.isFocused;
  }

  get hasError(): boolean {
    return this.control.hasError;
  }
}
