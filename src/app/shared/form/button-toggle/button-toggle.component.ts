import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  Optional,
  Renderer2,
  SkipSelf,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export enum DefaultToggleOption {
  None = '',
  On = 'on',
  Off = 'off'
}

@Component({
  selector: 'app-button-toggle',
  templateUrl: './button-toggle.component.html',
  styleUrls: ['./button-toggle.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ButtonToggleComponent,
      multi: true
    },
    {
      provide: DefaultToggleOption,
      useValue: DefaultToggleOption
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonToggleComponent implements ControlValueAccessor {
  @Input() name?: string;
  @ViewChild('control') private control: ElementRef;

  private readonly toggleOption: any; // Weird error with enum DefaultToggleOption
  private value: DefaultToggleOption;

  constructor(
    private renderer: Renderer2,
    private changeDetector: ChangeDetectorRef,
    defaultToggleOption: DefaultToggleOption,
    @Optional() @SkipSelf() globalToggleOption: DefaultToggleOption
  ) {
    this.toggleOption = globalToggleOption || defaultToggleOption;
    this.value = this.toggleOption.On;
  }

  get isOn(): boolean {
    return this.value === this.toggleOption.On;
  }

  get isActive(): boolean {
    return this.value !== this.toggleOption.None;
  }

  handleToggle(): void {
    const value = this.isOn ? this.toggleOption.Off : this.toggleOption.On;

    this.writeValue(value);
    this.handleChange(value);
  }

  writeValue(value: DefaultToggleOption): void {
    this.value = value;
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any): void {
    this.handleChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.handleTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.renderer.setProperty(this.control.nativeElement, 'disabled', isDisabled);
  }

  handleChange: (_: any) => void = () => {};

  handleTouched: () => void = () => {};
}
