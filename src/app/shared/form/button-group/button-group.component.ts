import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-button-group',
  template: '<ng-content></ng-content>',
  styleUrls: ['./button-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonGroupComponent {}
