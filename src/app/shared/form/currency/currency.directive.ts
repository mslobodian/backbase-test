import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

import { NumericInputValue } from './numeric-input-value';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'input[type="currency"]',
})
export class CurrencyDirective {

  constructor(
    private formControl: NgControl,
  ) {}

  @HostListener('input', ['$event'])
  onInput($event: Event): void {
    const value = new NumericInputValue(($event.currentTarget as HTMLInputElement).value);
    value.setFixedDecimals(2);

    const amount = value.toString();

    this.formControl.control.setValue(amount);
  }

  @HostListener('blur', ['$event'])
  onBlur($event: Event): void {
    const originalValue = ($event.currentTarget as HTMLInputElement).value;
    const value = new NumericInputValue(originalValue);
    value.setTrailingDecimalsZeros(2);

    const amount = value.toString();

    if (amount !== originalValue) {
      this.formControl.control.setValue(amount);
    }
  }
}
