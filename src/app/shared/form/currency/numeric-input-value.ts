export class NumericInputValue {

  static digitSeparator = '.';

  public integer: string = null;
  public decimal: string = null;

  static removeNonDecimal(value: string): string {
    return value.replace(/[^0-9]/g, '');
  }

  static sanitizeInteger(value: string): string {
    return value.replace(/0*(\d+)/g, '$1');
  }

  constructor(userInput: string | void | null) {
    const input = userInput || '';

    if (input === NumericInputValue.digitSeparator) {
      this.integer = '0';
      this.decimal = '';
    } else {
      const separatorPosition = input.indexOf(NumericInputValue.digitSeparator);

      if (separatorPosition > -1) {
        const [integer, decimal] = [
          input.slice(0, separatorPosition),
          input.slice(separatorPosition + 1)
        ].map(NumericInputValue.removeNonDecimal);

        this.integer = NumericInputValue.sanitizeInteger(integer);
        this.decimal = decimal;
      } else {
        this.integer = NumericInputValue.sanitizeInteger(NumericInputValue.removeNonDecimal(input));
        this.decimal = null;
      }
    }
  }

  toString(): string {
    return [this.integer, this.decimal]
      .filter((part: string) => part !== null)
      .join(NumericInputValue.digitSeparator);
  }

  valueOf(): number {
    const value = this.toString();

    if (value.endsWith(NumericInputValue.digitSeparator)) {
      return null;
    }

    const numeric = parseFloat(value);
    return isNaN(numeric) ? null : numeric;
  }

  setFixedDecimals(count: number): void {
    this.decimal = this.decimal && this.decimal.slice(0, count);
  }

  setTrailingDecimalsZeros(count: number): void {
    if (this.decimal || this.decimal  === '') {
      this.decimal = `${this.decimal}${new Array(count).fill(0).join('')}`.slice(0, count);
    }
  }
}
