import { Directive, ElementRef, HostListener, Optional } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Observable, of } from 'rxjs';

@Directive({
  selector: 'input[appInputRef]',
})
export class InputRefDirective {

  isFocused: boolean;

  constructor(
    @Optional() private formControl: NgControl,
    private elementRef: ElementRef
  ) {}

  clear(value = ''): void {
    this.formControl?.control.setValue(value);
  }

  get isCurrencyType(): boolean {
    return this.elementRef.nativeElement.getAttribute('type') === 'currency';
  }

  get value$(): Observable<string> {
    return this.formControl?.control.valueChanges || of();
  }

  get hasError(): boolean {
    const hasErrors = !!Object.values(this.formControl?.control.errors || {}).length;
    return this.formControl?.control.dirty && hasErrors;
  }

  @HostListener('focus')
  onFocus() {
    this.isFocused = true;
  }

  @HostListener('blur')
  onBlur() {
    this.isFocused = false;
  }
}
