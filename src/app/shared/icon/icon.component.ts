import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

type Icon = 'arrows' | 'briefcase';

@Component({
  selector: 'app-icon',
  template: '',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent {
  @Input() icon: Icon;

  @HostBinding('class')
  get hostClass(): string {
    return this.icon ? `icon-${this.icon}` : null;
  }
}
