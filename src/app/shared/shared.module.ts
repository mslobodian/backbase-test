import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LayoutComponent } from 'src/app/shared/layout/layout.component';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { TileHeadingComponent } from './tile/tile-heading/tile-heading.component';
import { TileBoxComponent } from './tile/tile-box/tile-box.component';
import { InputComponent } from './form/input/input.component';
import { InputErrorComponent } from './form/input-error/input-error.component';
import { CurrencyDirective } from './form/currency/currency.directive';
import { InputRefDirective } from './form/input-ref/input-ref.directive';
import { ButtonGroupComponent } from './form/button-group/button-group.component';
import { ButtonToggleComponent } from './form/button-toggle/button-toggle.component';
import { IconComponent } from './icon/icon.component';

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    TileBoxComponent,
    TileHeadingComponent,
    InputComponent,
    InputErrorComponent,
    CurrencyDirective,
    InputRefDirective,
    ButtonGroupComponent,
    ButtonToggleComponent,
    IconComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    LayoutComponent,
    TileBoxComponent,
    TileHeadingComponent,
    InputComponent,
    InputErrorComponent,
    CurrencyDirective,
    InputRefDirective,
    ButtonGroupComponent,
    ButtonToggleComponent,
    IconComponent
  ],
  providers: [],
})
export class SharedModule {}
