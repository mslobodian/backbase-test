const path = require('path');
const SpritesmithPlugin = require('webpack-spritesmith');

module.exports = {
  module: {
    rules: [
      {
        test: /\.png$/,
        use: [
          'file-loader?name=i/[hash].[ext]'
        ]
      }
    ]
  },
  plugins: [
    new SpritesmithPlugin({
      src: {
        cwd: path.resolve(__dirname, 'src/assets/icons'),
        glob: '*.png'
      },
      target: {
        image: path.resolve(__dirname, 'src/styles/sprite-generated/sprite.png'),
        css: path.resolve(__dirname, 'src/styles/sprite-generated/_sprite.scss')
      },
      apiOptions: {
        cssImageRef: 'src/styles/sprite-generated/sprite.png'
      }
    })
  ]
};
