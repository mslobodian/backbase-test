# BackbaseTest

This project is made with Angular 9.

To run application open `dist/backbase-test/index.html` with any simple server, for example:

```shell script
npx light-server -s . -b localhost -o
```

## What was done

##### Create transfer form
  - Implemented with Angular Reactive Forms.
  - Inputs
    - Custom inputs component with content projection.  
    - Custom validation on amount field. Balance after transfer should be above -$500.
    - All inputs are clearable (clear button appears on hover).
  - Custom currency directive.
    - Amount input accepts only digits.
    - After the dot there can be only two digits.  
    - When user input begins from the dot, leading zero will be added automatically.
  - Submit becomes enabled when form is valid.  

##### Transfer confirm state.
  - Implemented with Angular Routing. There are two buttons - "Confirm" or "Cancel".
  - Cancel button navigates back and prepopulates form with confirm transfer data.
  - Confirm button adds transfer to the transaction list and opens clear "Transfer Form".
    
##### Transaction List
  - Transactions can be ordered by 3 fields at the same time (asc/desc). Buttons are custom value accessors.
  - Search on every keystroke.
  - Search can be cleared with a button.
  
##### Data store
  - Classical store build on the BehaviourSubject.
  - Has an API that modifies data in asynchronous manner.
  
##### Project structure
  - There are 3 classical folders in `app`: core, modules and shared.
  - Global styles and scss mixins/variables (in project known as `abstract`) are located in `styles`
  - All static files are located in `assets`.
  
##### Other feature
  - Icons are merged into sprite. 
